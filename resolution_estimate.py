# -*- coding: utf-8 -*-
"""
Created on Mon Jun 15 12:20:36 2015

@author: Julio Cesar da Silva (mailto: jdasilva@esrf.fr)
"""

from __future__ import division, print_function
import h5py
from scipy import *
import numpy as np
import matplotlib.pyplot as plt
import os

#from skimage.measure import profile_line
from skimage.feature import register_translation
from skimage.feature.register_translation import _upsampled_dft
from scipy.ndimage.fourier import fourier_shift
from scipy.special import erf
from Tkinter import Tk
from tkFileDialog import askopenfilename, askopenfilenames
from time import time
from FSC import *

Tk().withdraw()
pathfilename1 = askopenfilename(initialdir='.')#'/data/id16a/inhouse1/commissioning/')      
pathfilename2 = askopenfilename(initialdir='.')
#filename = pathfilename.rsplit('/')[-1]
#path = pathfilename[:pathfilename.find(pathfilename.rsplit('/')[-1])]
#print(path)

# Read the HDF5 file .ptyr
file1 = h5py.File(pathfilename1)#'id16siemensstar_2_ML.ptyr','r')
file2 = h5py.File(pathfilename2)#'id16siemensstarLH_140nm_10s_ML.ptyr','r')

# get the root entry
content1 = file1.keys()[0]
content2 = file2.keys()[0]

# get the data from the object
data1 = np.squeeze(np.array(file1[content1+"/obj/S00G00/data"]))
data2 = np.squeeze(np.array(file2[content2+"/obj/S00G00/data"]))

# get the pixel size
pixelsize1 = file1[content1+"/obj/S00G00/_psize"].value
pixelsize2 = file2[content2+"/obj/S00G00/_psize"].value

print("the pixelsize of data1 is %.2f nm" %(pixelsize1[0]*1e9))
print("the pixelsize of data2 is %.2f nm" %(pixelsize2[0]*1e9))

# Close the files, which are not needed anymore
file1.close()
file2.close()

# cropping the image to an useful area
reg = 110 # how many pixels are cropped from the borders of the image
image1 = np.angle(data1[0+reg:-1-reg,0+reg:-1-reg])
image2 = np.angle(data2[0+reg:-1-reg,0+reg:-1-reg])

# Choose between pixel or subpixel precision image registration. By default, it is pixel precision.
precision = (raw_input("Do you want to use pixel(1) or subpixel(2) precision registration?[1] "))
if (precision==str(1) or precision ==''):
    # pixel precision
    print("\nCalculating the pixel precision image registration ...")
    start = time()
    shift, error, diffphase = register_translation(image1, image2)
    end = time()
    print("Time elapsed: %g s" %(end-start))
    print("Detected pixel offset [y,x]: [%g, %g]" %(shift[0],shift[1]))
elif precision == str(2):
    # subpixel precision
    print("\nCalculating the subpixel image registration ...")
    start = time()
    shift, error, diffphase = register_translation(image1, image2, 100)
    end = time()
    print("Time elapsed: %g s" %(end-start))
    print("Detected subpixel offset [y,x]: [%g, %g]" %(shift[0],shift[1]))
else:
    print("You must choose between 1 and 2")
    raise SystemExit

#print("Shift = [%g, %g], Error= %g, DiffPhase = %g" %(shift[0],shift[1],error,diffphase))

# Display the images
fig, (ax1, ax2, ax3) = plt.subplots(ncols=3, figsize=(14,6))
ax1.imshow(image1,origin='lower',interpolation='none')
ax1.set_axis_off()
ax1.set_title('Image 1 (ref.)')
ax2.imshow(image2,origin='lower',interpolation='none')
ax2.set_axis_off()
ax2.set_title('Image 2')

# View the output of a cross-correlation to show what the algorithm is
# doing behind the scenes
image_product = np.fft.fft2(image1) * np.fft.fft2(image2).conj()
cc_image = np.fft.fftshift(np.fft.ifft2(image_product))
ax3.imshow(cc_image.real)
#ax3.set_axis_off()
ax3.set_title("Cross-correlation")

plt.show()

print("\nCorrecting the shift of image2 by using subpixel precision...")
offset_image2 = np.fft.ifftn(fourier_shift(np.fft.fftn(image2),shift))

fig, (ax1, ax2) = plt.subplots(ncols=2, figsize=(14, 6))

ax1.imshow(image1,origin='lower',interpolation='none')
ax1.set_axis_off()
ax1.set_title('Image1 (ref.)')

ax2.imshow(offset_image2.real,origin='lower',interpolation='none')
ax2.set_axis_off()
ax2.set_title('Offset corrected image2')

plt.show()

print("Estimating the resolution by FSC...")
startfsc = time()
regfsc=np.max(shift)
FSC2D=FSCPlot(image1[0+regfsc:-1-regfsc,0+regfsc:-1-regfsc],offset_image2.real[0+regfsc:-1-regfsc,0+regfsc:-1-regfsc],0.5,4)
FSC2D.plot(save_fig=True)
#fig.savefig('FSC.png', bbox_inches='tight')
endfsc = time()
print("Time elapsed: %g s" %(endfsc-startfsc))

print("The pixelsize of the data is %.2f nm" %(pixelsize1[0]*1e9))

a = raw_input("\nPlease, input the value of the intersection: ")
print("------------------------------------------")
print("| Resolution is estimated to be %.2f nm |" %(pixelsize1[0]*1e9/float(a)))
print("------------------------------------------")

raw_input("\n<Hit Return to close images>")
plt.close('all')

print("Your program has finished!")
_=os.system('say "Your program has finished!Goodbye"')

